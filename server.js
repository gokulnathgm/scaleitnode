const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
//const mongoUrl = 'mongodb://10.7.80.3:27017/nataraj';
const mongoUrl = 'mongodb://localhost/nataraj';
const routes = require('./app/routes/index');
const WebSocketServer = require('websocket').server;
const testService = require('./app/services/test.service');
const cluster = require('cluster');
// const machineId = require( "ee-machine-id" );
const myArgs = process.argv.slice(2);
const port = myArgs[0];
console.log(myArgs[0]);
let workerId;
let machineId = '_vishnupe'+myArgs[0];


// machineId.get( function( id ){
//   //console.log( id ); // 5ccbe155d5440cd06e3664c0ae3e811e
//   machineId = id;
// } );


if (cluster.isWorker) {
  workerId = cluster.worker.id;
}

// function generateChannel(event, id) {
//   var channel = event + '_' + id + '_vishnuc';
//   return channel;
// }

// const request = require('redis-event-queue')({ host: "127.0.0.1", port: "6379" });
// let clientQueue = request.workqueue;

// clientQueue.on(generateChannel('send_message', workerId), sendMessageCallback);
// clientQueue.on(generateChannel('close_client', workerId), closeClientCallback);
// //for generating log
// clientQueue.on('apiLog', apiLogCallback);
// clientQueue.on('chatLog', chatLogCallback);

// function apiLogCallback(msg){
//   //console.log('api log channel ON');
// }

// function chatLogCallback(msg){
//   //console.log('chat log channel ON');
// }

// function sendMessageCallback(msg) {
//   //console.log('send message at #', workerId);
// }

// function closeClientCallback(client) {
//   //console.log('close client at #', workerId);
// }



mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl, function(err, res) {
  if (err) {
    console.log(err);
    console.log('Worker #' + (workerId || '') + ' Mongo connection failed');
  } else {
    console.log('Worker #' + (workerId || '') + ' Mongo connection success');


  }
});


app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/log'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//setting routes
routes(app);

const server = app.listen(port, function() {
  console.log('Worker #' + (workerId || '') + ' Server Started'+port);
  testService.listenForAllEvents(workerId,machineId);
});
wsServer = new WebSocketServer({
  httpServer: server
});


let count = 0;


// Deal with socket client requests
wsServer.on('request', function(r) {
  testService.connectClient(r, workerId,machineId);
  count++;
  //console.log('worker '+workerId+'>>>'+count);
  //testService.connectClient(r);
  // if valid token
  // if connected => get worker id => close_client_workerId
  // accept
  ////console.log('socket connection worker', workerId, r.resourceURL.query.token);
  // clientQueue.emit(generateChannel(workerId),{data:'data'});
});
