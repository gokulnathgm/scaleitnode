var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "17150",
        "ok": "17140",
        "ko": "10"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "11518",
        "ok": "11518",
        "ko": "10020"
    },
    "meanResponseTime": {
        "total": "3150",
        "ok": "3146",
        "ko": "10015"
    },
    "standardDeviation": {
        "total": "3326",
        "ok": "3323",
        "ko": "3"
    },
    "percentiles1": {
        "total": "2234",
        "ok": "2231",
        "ko": "10016"
    },
    "percentiles2": {
        "total": "5037",
        "ok": "5033",
        "ko": "10018"
    },
    "percentiles3": {
        "total": "9981",
        "ok": "9966",
        "ko": "10019"
    },
    "percentiles4": {
        "total": "11192",
        "ok": "11192",
        "ko": "10020"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 6456,
        "percentage": 38
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 673,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 10011,
        "percentage": 58
    },
    "group4": {
        "name": "failed",
        "count": 10,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "535.938",
        "ok": "535.625",
        "ko": "0.312"
    }
},
contents: {
"req_sender-sign-in-cfca0": {
        type: "REQUEST",
        name: "Sender Sign In",
path: "Sender Sign In",
pathFormatted: "req_sender-sign-in-cfca0",
stats: {
    "name": "Sender Sign In",
    "numberOfRequests": {
        "total": "2450",
        "ok": "2450",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11511",
        "ok": "11511",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4393",
        "ok": "4393",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2912",
        "ok": "2912",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6001",
        "ok": "6001",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10377",
        "ok": "10377",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11298",
        "ok": "11298",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 158,
        "percentage": 6
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 157,
        "percentage": 6
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2135,
        "percentage": 87
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "76.562",
        "ok": "76.562",
        "ko": "-"
    }
}
    },"req_receiver-sign-i-4989f": {
        type: "REQUEST",
        name: "Receiver Sign In",
path: "Receiver Sign In",
pathFormatted: "req_receiver-sign-i-4989f",
stats: {
    "name": "Receiver Sign In",
    "numberOfRequests": {
        "total": "2450",
        "ok": "2450",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11518",
        "ok": "11518",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4370",
        "ok": "4370",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3731",
        "ok": "3731",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6007",
        "ok": "6007",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10411",
        "ok": "10411",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11295",
        "ok": "11295",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 181,
        "percentage": 7
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 147,
        "percentage": 6
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2122,
        "percentage": 87
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "76.562",
        "ok": "76.562",
        "ko": "-"
    }
}
    },"req_receiver-open-w-d083d": {
        type: "REQUEST",
        name: "Receiver Open WS",
path: "Receiver Open WS",
pathFormatted: "req_receiver-open-w-d083d",
stats: {
    "name": "Receiver Open WS",
    "numberOfRequests": {
        "total": "2450",
        "ok": "2450",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11509",
        "ok": "11509",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4876",
        "ok": "4876",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4161",
        "ok": "4161",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8085",
        "ok": "8085",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10717",
        "ok": "10717",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11344",
        "ok": "11344",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 266,
        "percentage": 11
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 80,
        "percentage": 3
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2104,
        "percentage": 86
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "76.562",
        "ok": "76.562",
        "ko": "-"
    }
}
    },"req_sender-open-ws-f9657": {
        type: "REQUEST",
        name: "Sender Open WS",
path: "Sender Open WS",
pathFormatted: "req_sender-open-ws-f9657",
stats: {
    "name": "Sender Open WS",
    "numberOfRequests": {
        "total": "2450",
        "ok": "2450",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11506",
        "ok": "11506",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3408",
        "ok": "3408",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4314",
        "ok": "4314",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8265",
        "ok": "8265",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10802",
        "ok": "10802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11338",
        "ok": "11338",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 245,
        "percentage": 10
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 70,
        "percentage": 3
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2135,
        "percentage": 87
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "76.562",
        "ok": "76.562",
        "ko": "-"
    }
}
    },"req_receive-message-8e76e": {
        type: "REQUEST",
        name: "Receive Messages",
path: "Receive Messages",
pathFormatted: "req_receive-message-8e76e",
stats: {
    "name": "Receive Messages",
    "numberOfRequests": {
        "total": "2450",
        "ok": "2442",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "10020",
        "ok": "9907",
        "ko": "10020"
    },
    "meanResponseTime": {
        "total": "1803",
        "ok": "1776",
        "ko": "10015"
    },
    "standardDeviation": {
        "total": "2638",
        "ok": "2600",
        "ko": "3"
    },
    "percentiles1": {
        "total": "155",
        "ok": "154",
        "ko": "10015"
    },
    "percentiles2": {
        "total": "3272",
        "ok": "3175",
        "ko": "10016"
    },
    "percentiles3": {
        "total": "7365",
        "ok": "7324",
        "ko": "10019"
    },
    "percentiles4": {
        "total": "9184",
        "ok": "8927",
        "ko": "10020"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1549,
        "percentage": 63
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 115,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 778,
        "percentage": 32
    },
    "group4": {
        "name": "failed",
        "count": 8,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "76.562",
        "ok": "76.312",
        "ko": "0.25"
    }
}
    },"req_send-messages-bb998": {
        type: "REQUEST",
        name: "Send Messages",
path: "Send Messages",
pathFormatted: "req_send-messages-bb998",
stats: {
    "name": "Send Messages",
    "numberOfRequests": {
        "total": "2450",
        "ok": "2450",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles2": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles3": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles4": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 2450,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "76.562",
        "ok": "76.562",
        "ko": "-"
    }
}
    },"req_send-messages-c-d268e": {
        type: "REQUEST",
        name: "Send Messages Check",
path: "Send Messages Check",
pathFormatted: "req_send-messages-c-d268e",
stats: {
    "name": "Send Messages Check",
    "numberOfRequests": {
        "total": "2450",
        "ok": "2448",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "3",
        "ok": "3",
        "ko": "10018"
    },
    "maxResponseTime": {
        "total": "10018",
        "ok": "9756",
        "ko": "10018"
    },
    "meanResponseTime": {
        "total": "1565",
        "ok": "1558",
        "ko": "10018"
    },
    "standardDeviation": {
        "total": "2503",
        "ok": "2493",
        "ko": "0"
    },
    "percentiles1": {
        "total": "37",
        "ok": "37",
        "ko": "10018"
    },
    "percentiles2": {
        "total": "2424",
        "ok": "2417",
        "ko": "10018"
    },
    "percentiles3": {
        "total": "7156",
        "ok": "7141",
        "ko": "10018"
    },
    "percentiles4": {
        "total": "8596",
        "ok": "8530",
        "ko": "10018"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1607,
        "percentage": 66
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 104,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 737,
        "percentage": 30
    },
    "group4": {
        "name": "failed",
        "count": 2,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "76.562",
        "ok": "76.5",
        "ko": "0.062"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
