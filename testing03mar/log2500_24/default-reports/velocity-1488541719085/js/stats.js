var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "17500",
        "ok": "17476",
        "ko": "24"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "11448",
        "ok": "11448",
        "ko": "10653"
    },
    "meanResponseTime": {
        "total": "3100",
        "ok": "3090",
        "ko": "10111"
    },
    "standardDeviation": {
        "total": "3315",
        "ok": "3307",
        "ko": "203"
    },
    "percentiles1": {
        "total": "2146",
        "ok": "2133",
        "ko": "10017"
    },
    "percentiles2": {
        "total": "4960",
        "ok": "4951",
        "ko": "10044"
    },
    "percentiles3": {
        "total": "9997",
        "ok": "9952",
        "ko": "10636"
    },
    "percentiles4": {
        "total": "11122",
        "ok": "11123",
        "ko": "10651"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 6714,
        "percentage": 38
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 629,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 10133,
        "percentage": 58
    },
    "group4": {
        "name": "failed",
        "count": 24,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "546.875",
        "ok": "546.125",
        "ko": "0.75"
    }
},
contents: {
"req_sender-sign-in-cfca0": {
        type: "REQUEST",
        name: "Sender Sign In",
path: "Sender Sign In",
pathFormatted: "req_sender-sign-in-cfca0",
stats: {
    "name": "Sender Sign In",
    "numberOfRequests": {
        "total": "2500",
        "ok": "2500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11448",
        "ok": "11448",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4322",
        "ok": "4322",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2942",
        "ok": "2942",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3733",
        "ok": "3733",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5900",
        "ok": "5900",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10306",
        "ok": "10306",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11216",
        "ok": "11216",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 211,
        "percentage": 8
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 127,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2162,
        "percentage": 86
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "78.125",
        "ok": "78.125",
        "ko": "-"
    }
}
    },"req_receiver-sign-i-4989f": {
        type: "REQUEST",
        name: "Receiver Sign In",
path: "Receiver Sign In",
pathFormatted: "req_receiver-sign-i-4989f",
stats: {
    "name": "Receiver Sign In",
    "numberOfRequests": {
        "total": "2500",
        "ok": "2500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "57",
        "ok": "57",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11439",
        "ok": "11439",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3742",
        "ok": "3742",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5959",
        "ok": "5959",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10343",
        "ok": "10343",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11218",
        "ok": "11218",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 243,
        "percentage": 10
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 116,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2141,
        "percentage": 86
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "78.125",
        "ok": "78.125",
        "ko": "-"
    }
}
    },"req_sender-open-ws-f9657": {
        type: "REQUEST",
        name: "Sender Open WS",
path: "Sender Open WS",
pathFormatted: "req_sender-open-ws-f9657",
stats: {
    "name": "Sender Open WS",
    "numberOfRequests": {
        "total": "2500",
        "ok": "2500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11432",
        "ok": "11432",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4934",
        "ok": "4934",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4122",
        "ok": "4122",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8233",
        "ok": "8233",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10780",
        "ok": "10780",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11292",
        "ok": "11292",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 269,
        "percentage": 11
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 71,
        "percentage": 3
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2160,
        "percentage": 86
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "78.125",
        "ok": "78.125",
        "ko": "-"
    }
}
    },"req_receiver-open-w-d083d": {
        type: "REQUEST",
        name: "Receiver Open WS",
path: "Receiver Open WS",
pathFormatted: "req_receiver-open-w-d083d",
stats: {
    "name": "Receiver Open WS",
    "numberOfRequests": {
        "total": "2500",
        "ok": "2500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11430",
        "ok": "11430",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4866",
        "ok": "4866",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3381",
        "ok": "3381",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4151",
        "ok": "4151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8119",
        "ok": "8119",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10600",
        "ok": "10600",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11265",
        "ok": "11265",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 295,
        "percentage": 12
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 57,
        "percentage": 2
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2148,
        "percentage": 86
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "78.125",
        "ok": "78.125",
        "ko": "-"
    }
}
    },"req_send-messages-bb998": {
        type: "REQUEST",
        name: "Send Messages",
path: "Send Messages",
pathFormatted: "req_send-messages-bb998",
stats: {
    "name": "Send Messages",
    "numberOfRequests": {
        "total": "2500",
        "ok": "2500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles2": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles3": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles4": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 2500,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "78.125",
        "ok": "78.125",
        "ko": "-"
    }
}
    },"req_send-messages-c-d268e": {
        type: "REQUEST",
        name: "Send Messages Check",
path: "Send Messages Check",
pathFormatted: "req_send-messages-c-d268e",
stats: {
    "name": "Send Messages Check",
    "numberOfRequests": {
        "total": "2500",
        "ok": "2500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9920",
        "ok": "9920",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1534",
        "ok": "1534",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6611",
        "ok": "6611",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7801",
        "ok": "7801",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1635,
        "percentage": 65
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 101,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 764,
        "percentage": 31
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "78.125",
        "ok": "78.125",
        "ko": "-"
    }
}
    },"req_receive-message-8e76e": {
        type: "REQUEST",
        name: "Receive Messages",
path: "Receive Messages",
pathFormatted: "req_receive-message-8e76e",
stats: {
    "name": "Receive Messages",
    "numberOfRequests": {
        "total": "2500",
        "ok": "2476",
        "ko": "24"
    },
    "minResponseTime": {
        "total": "2",
        "ok": "2",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "10653",
        "ok": "9996",
        "ko": "10653"
    },
    "meanResponseTime": {
        "total": "1748",
        "ok": "1667",
        "ko": "10111"
    },
    "standardDeviation": {
        "total": "2613",
        "ok": "2492",
        "ko": "203"
    },
    "percentiles1": {
        "total": "160",
        "ok": "158",
        "ko": "10017"
    },
    "percentiles2": {
        "total": "2826",
        "ok": "2663",
        "ko": "10044"
    },
    "percentiles3": {
        "total": "7321",
        "ok": "6903",
        "ko": "10636"
    },
    "percentiles4": {
        "total": "9965",
        "ok": "8833",
        "ko": "10651"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1561,
        "percentage": 62
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 157,
        "percentage": 6
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 758,
        "percentage": 30
    },
    "group4": {
        "name": "failed",
        "count": 24,
        "percentage": 1
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "78.125",
        "ok": "77.375",
        "ko": "0.75"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
