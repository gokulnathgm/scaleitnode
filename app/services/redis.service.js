const redis = require("redis");
const bluebird = require('bluebird')
bluebird.promisifyAll(redis.RedisClient.prototype);
//const client = redis.createClient({ host: "10.7.80.3", port: "6379" });
const client = redis.createClient();

var machineId = require( "ee-machine-id" );

////console.log( machineId.id ); // undefined because the information was not laoded yet

machineId.get( function( id ){
	////console.log( id ); // 5ccbe155d5440cd06e3664c0ae3e811e
} );


function addClientToList(name,socket){
	client.hmset("clients",name,socket.toString());
}
function getClients(){
	client.hgetall("clients", function (err, obj) {
	    ////console.log(JSON.parse(obj[0]));
	});
}
module.exports = {
	addClientToList: addClientToList,
	getClients: getClients
}