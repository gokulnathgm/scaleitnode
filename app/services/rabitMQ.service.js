const cluster = require('cluster');
var rabbitmq = require('rabbitmq-eventemitter');
var clientQueue = rabbitmq('amqp://localhost');

let workerId;


if (cluster.isWorker) {
  workerId = cluster.worker.id;
}

function generateChannel(event, id, machineId) {
  var channel = event + '_' + id + machineId;
  ////console.log(channel);
  return channel;
}


function emitEvent(event, workerId, machineId, data) {
  clientQueue.push(generateChannel(event, workerId, machineId), data);
  console.log(event);
}

function listenEvent(event, workerId, machineId, callback) {
  clientQueue.pull(generateChannel(event, workerId, machineId), callback);
  console.log(event);
}

// custom event listners
function listenCloseClient(workerId, machineId, closeClientCallback) {
  listenEvent('close_client', workerId, machineId, closeClientCallback);
}

function listenSendMessage(workerId, machineId, sendMessageCallback) {
  listenEvent('send_message', workerId, machineId, sendMessageCallback);
}

// custom event emiters
function emitCloseClient(user, workerId, id, machineId) {
  emitEvent('close_client', workerId, machineId, JSON.stringify({ user, id }));
}

function emitSendMessage(workerId, machineId, message) {
  ////console.log('am here inside emitSendMessage');
  emitEvent('send_message', workerId, machineId, message);
}


function apiLogListener(apiLogListenerCallback) {
  ////console.log('am here inside apiLogListener');
  clientQueue.pull('apilog', apiLogListenerCallback);
}

function generateApiLog() {

  clientQueue.push('apilog', 'generateApiLog');
}

function chatLogListener(chatLogListenerCallback) {

  clientQueue.pull('chatLog', chatLogListenerCallback);
}

function generateChatLog() {
  clientQueue.push('chatLog', 'generateChatLog');
}


function incomingMessageListner(incomingMessageListenerCallback) {
  clientQueue.pull('incomingMessage', incomingMessageListenerCallback);
}

function generateincomingMessageLog() {
  clientQueue.push('incomingMessage', 'generateIncomingMessageLog');
}


function filterMessageListner(filterMessageListenerCallback) {
  clientQueue.pull('generatefilterMessage', filterMessageListenerCallback);
}

function generatefilterMessageLog() {
  clientQueue.push('generatefilterMessage', 'generatefilterMessageLog');
}

function connectClientListener(connectClientListenerCallback) {
  clientQueue.pull('connect_client', connectClientListenerCallback);
}

function emitConnectClient(user, workerId, id, machineId) {
  clientQueue.push('connect_client', JSON.stringify({'user':user, 'workerId':workerId,'id':id, 'machineId':machineId}));
}

module.exports = {
  listenCloseClient,
  listenSendMessage,
  emitCloseClient,
  emitSendMessage,
  apiLogListener,
  chatLogListener,
  generateApiLog,
  generateChatLog,
  incomingMessageListner,
  generateincomingMessageLog,
  filterMessageListner,
  generatefilterMessageLog,
  connectClientListener,
  emitConnectClient
}
