let activeConnections = {};

function addConnection(name, connection) {
  activeConnections[name] = connection;
}

function removeConnection(name) {
  delete activeConnections[name];
}

function getActiveConnections() {
  return activeConnections;
}

function getConnection(name) {

  return activeConnections[name];
}

module.exports = {
  addConnection,
  removeConnection,
  getActiveConnections,
  getConnection
}
