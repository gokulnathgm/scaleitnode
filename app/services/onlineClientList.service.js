let onlineClientList = {};
function addNewClient(user, instanceId) {
  onlineClientList[user]={
  	instanceId
  };
}
function getClientInfo(user) {
  return onlineClientList[user];
}
function removeClient(user) {
  delete onlineClientList[user]
}
function getCount() {
	return Object.keys(onlineClientList).length;
}
module.exports = {
  addNewClient,
  getClientInfo,
  removeClient,
  getCount
}