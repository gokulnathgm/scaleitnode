const redis = require("redis");
const bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
//const client = redis.createClient({ host: "10.7.80.3", port: "6379" });
const client = redis.createClient();


client.on('connect', function() {
  ////console.log('redis client started')
})

//saving username and token as key-value pair in redis
function addNewToken(name, token) {
  return client.hmsetAsync('activeTokens', { [name]: token });
}

function getUserToken(name) {
  return client.hgetAsync('activeTokens', name);
}


function removeToken(name) {
  return client.hdelAsync('activeTokens', name);
}





module.exports = {
  addNewToken: addNewToken,
  getUserToken: getUserToken,
  removeToken: removeToken,

}
