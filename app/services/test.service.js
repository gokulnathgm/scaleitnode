// const jsonTocsv = require('../services/json2csv.service');
// ////console.log(jsonTocsv);
const tokenListService = require('./tokenList.service.js');
const connectionListService = require('./connectionList.service.js');
const activeClientListService = require('./activeClientList.service.js');
const userService = require('../services/user.service');
const eventQueueService = require('../services/eventQueue.service');
// const eventQueueService = require('../services/rabitMQ.service');
const shortid = require('shortid');
const activeClientLocalListService = require('./activeClientLocalList.service.js');
const tokenLocalListService = require('./tokenLocalList.service.js');

const jwt = require('jsonwebtoken');
const Promise = require('bluebird');
const jwtVerifyAsync = Promise.promisify(jwt.verify, jwt);
const bodyParser = require('body-parser');
const json2csv = require('json2csv');
let fs = require('fs');
let flag = 0;
let apiLogArray = [];
let chatLogArray = [];
let incomingMessageArray = [];

let errorMessageArray = [];
let connections = 0;
let noconnections = 0;
let closedConnections = 0;

function filterMessages(workerId, machineId) {
  let fields = ['From IP', 'Sender', 'Receiver', 'Mesage', 'Received At (Timestamp)', 'Sent At (Timestamp)','sId'];
  // let id = shortid.generate();

  id = 'filterMsg Log_' + machineId + workerId + '.csv';
  loggenerator(id, incomingMessageArray, fields);
}

//check failed messages

function filterMessagesLogGenerator(workerId, machineId) {
  //console.log('am here', incomingMessageArray, chatLogArray)
  let chatLogArrayLength = chatLogArray.length
  let incomingMessageArrayLength = incomingMessageArray.length

  if (chatLogArrayLength !== 0) {
    for (i = 0; i < incomingMessageArray.length; i++) {
      for (j = 0; j < chatLogArrayLength; j++) {
        if (incomingMessageArray[i]['Received At (Timestamp)'] === chatLogArray[j]['Received At (Timestamp)']) {
          incomingMessageArray.splice(i)
          i--;
          break
        }
      }
    }
  }
  //console.log('\nErrorss>>>>>>>>>>>>>>>>>>>>:',errorMessageArray.length);
  console.log('\nErrorss>>>>>>>>>>>>>>>>>>>>:',errorMessageArray);
  console.log('\nConnections>>>>'+activeClientLocalListService.getCount());
  console.log('\nNoConnections>>>>'+noconnections);
  console.log('\nClosed connections>>>'+closedConnections)
  filterMessages(workerId, machineId);
}










function extractTokenFromRequest(request) {

  return request.resourceURL.query.token;
}

function isValidMessage(message) {

  return message.length <= 2000;
}

function isValidUser(name) {

  return userService.findOnePromise({ username: name })
}

function isValidToken(token) {

  return jwtVerifyAsync(token, 'jaratan');
}

function apiLog(req, recievedTime, sendTime, response, boolean) {
  apiLogArray.push({
    'From IP': req.connection.remoteAddress.replace(/^.*:/, ''),
    'API URL': req.protocol + '://' + req.get('host') + req.originalUrl,
    'JSON Parameter': req.body,
    'Received At (Timestamp)': recievedTime,
    'Response': response,
    'Sent At (Timestamp)': sendTime,
    'Success': boolean
  });
  ////console.log(apiLogArray);

}

function apiLogGenerator(workerId, machineId) {
  let fields = ['From IP', 'API URL', 'JSON Parameter', 'Received At (Timestamp)', 'Response', 'Sent At (Timestamp)', 'Success'];
  // let id = shortid.generate();
  //console.log(process.cwd());
  id = 'API Log_' + machineId + workerId + '.csv';
  loggenerator(id, apiLogArray, fields);
}

function chatLog(messageLogData) {
  //console.log(messageLogData)
  chatLogArray.push({
    'From IP': messageLogData.data.clientIp,
    'Sender': messageLogData.data.author,
    'Receiver': messageLogData.data.to,
    'Mesage': messageLogData.data.text,
    'Received At (Timestamp)': messageLogData.data.requestTime,
    'Sent At (Timestamp)': messageLogData.data.sendTime,
    'sId':messageLogData.data.sId
  });
}


function chatLogGenerator(workerId, machineId) {
  let fields = ['From IP', 'Sender', 'Receiver', 'Mesage', 'Received At (Timestamp)', 'Sent At (Timestamp)','sId'];
  // let id = shortid.generate();

  id = 'CHAT Log_' + machineId + workerId + '.csv';
  loggenerator(id, chatLogArray, fields);
}



function incomingMessageLogGenerator(workerId, machineId) {
  let fields = ['From IP', 'Sender', 'Receiver', 'Mesage', 'Received At (Timestamp)', 'Sent At (Timestamp)','sId'];
  // let id = shortid.generate();

  id = 'IM Log_' + machineId + workerId + '.csv';
  loggenerator(id, incomingMessageArray, fields);
}



function incomingMessageLog(messageLogData) {
  messageLogData = JSON.parse(messageLogData)
  incomingMessageArray.push({
    'From IP': messageLogData.data.clientIp,
    'Sender': messageLogData.data.author,
    'Receiver': messageLogData.data.to,
    'Mesage': messageLogData.data.text,
    'Received At (Timestamp)': messageLogData.data.requestTime,
    'Sent At (Timestamp)': 'NA',
    'sId':messageLogData.data.sId

  });
  //console.log('incomingMessageArray');

}


function loggenerator(name, data, fields) {
  let csv = json2csv({ data: data, fields: fields });
  fs.writeFile(name, csv, function(err) {
    if (err) throw err;
    console.log('file saved');
  });
}



// pani baakkiyund
function connectClient(request, swanthamWorkerId, swanthamMachineId) {

  //extract token from request
  let token = extractTokenFromRequest(request);

  isValidToken(token).then(function(decoded) {
    //extract name from token
    let name = decoded.name;
    tokenListService.getUserToken(name).then(function(activeToken) {
      if (token === activeToken) {
        /*activeClientListService.getWorkerInfo(name).then(function(Ids) {
        let Ids = activeClientLocalListService.getWorkerInfo(name);
           let userIds = JSON.parse(Ids);
          let userIds = Ids;
          if (Ids !== null) {
            if (userIds.machineId === swanthamMachineId && userIds.workerId === swanthamWorkerId) {
              let conn = connectionListService.getConnection(name);
              conn.close();
            } else {
              eventQueueService.emitCloseClient(name, userIds.workerId, userIds.id, userIds.machineId);
            }

          }

          // ividoru avalokanam venammm..........

          
          
          
          
        });*/
        let connection = request.accept(null, request.origin);
          //console.log('Connection successfull');

          let id = shortid.generate();
          connectionListService.addConnection(name, connection);
          eventQueueService.emitConnectClient(name, swanthamWorkerId, id, swanthamMachineId);
          activeClientListService.addNewClient(name, swanthamWorkerId, id, swanthamMachineId).then(function (argument) {
            connections++;
          });
          prepareConnection(connection, name, id, swanthamWorkerId, swanthamMachineId);
      } else {
        //console.log('Invalid Token');
        noconnections++;
      }
    })

  }).catch(function (err){
    console.log('gfgagf>>>>>>>>>>>>>>',token,err);
  });
}


// peru mattanam && on close handle cheyyanam mikkavarum function thattendi varum
function prepareConnection(connection, username, uniqueId, workerId, machineId) {

  connection.on('message', function(message) {
    let clientIp = connection.remoteAddresses[0].replace(/^.*:/, '');
    // message recieved time (on server)
    let requestTime = new Date().getTime();
    let messageObject = JSON.parse(message.utf8Data);
    let receiverName = messageObject.to;
    let messageString = messageObject.msg;

    // verify message length
    if (isValidMessage(messageString)) {

      //check if valid user in mongo
      //let response = isValidUser(receiverName).then(function(user) {

        

          // check if user online redis
          // return activeClientListService.getWorkerInfo(receiverName)
          let result = activeClientLocalListService.getWorkerInfo(receiverName)
          //console.log(result,activeClientLocalListService.getCount());
            if (result!==undefined){
              let workerId = result.workerId;
              let machineId = result.machineId;
              //console.log(workerId);
              let sId = shortid.generate();
              let message = JSON.stringify({
                  type: 'message',
                  data: {
                    text: messageString,
                    to: receiverName,
                    author: username,
                    requestTime,
                    clientIp,
                    sId

                  }
                })
              //console.log(message);
              eventQueueService.emitSendMessage(workerId, machineId, message);
              connection.sendUTF(message);
              //console.log(message);
              incomingMessageLog(message);
          }else{
            
            connection.send(JSON.stringify({ type: 'alert', data: 'The user is not available' }));
              /*let response = isValidUser(receiverName).then(function(user) {
                if(user){
                  let sendTime = new Date().getTime()
                  connection.send(JSON.stringify({ type: 'alert', data: 'The user is not available' }));
                  errorMessageArray.push({message,error:{ type: 'alert', data: 'The user is not available' }});
                  //errorMessageArray.push({receiverName})
                }else {
                  let sendTime = new Date().getTime()
                  connection.send(JSON.stringify({ type: 'alert', data: 'The user doesnt exists' }));
                  errorMessageArray.push({message,error:{ type: 'alert', data: 'The user doesnt exists' }});
                  
                }
              }) */          
          }
        
        
      //})
    } else {
      //console.log('The message length exceeded');
      connection.send(JSON.stringify({ type: 'alert', data: 'The message length exceeded' }));
      errorMessageArray.push({message,error:{ type: 'alert', data: 'The message length exceeded' }});
    }
  })
  connection.on('close', function(reasonCode, description) {
    console.log('connection closed', reasonCode, description,new Date().getTime());
    closedConnections++;
    activeClientListService.getWorkerInfo(username).then(function(info) {
      let userinfo = JSON.parse(info);
      if (userinfo.id === uniqueId) {
        //redis remove
        /*activeClientListService.removeClient(username).then(function() {

          // connectionListService.removeConnection(name);
          // handle local remove if closed at client side
        });*/


      }
    })
  });
}


function listenForAllEvents(swanthamWorkerId, swanthamMachineId) {
  //console.log('listening event in ' + swanthamWorkerId);
  eventQueueService.listenCloseClient(swanthamWorkerId, swanthamMachineId, function(info) {
    let data = JSON.parse(info)
    let conn = connectionListService.getConnection(data.user);
    conn.close();
  });

  eventQueueService.listenSendMessage(swanthamWorkerId, swanthamMachineId, function(message1) {
    let message = JSON.parse(message1);
    let username = message.data.to;
    message.data.time = new Date().getTime();
    let messageLogData = JSON.parse(JSON.stringify(message));
    delete message.data.requestTime;
    delete message.data.clientIp;
    delete message.data.sId;
    message1 = JSON.stringify(message);
    let conn = connectionListService.getConnection(username);
    let sendTime = new Date().getTime();
    conn.send(message1);
    messageLogData.data.sendTime = sendTime;
    chatLog(messageLogData);

  })
  eventQueueService.apiLogListener(function(message) {
    //console.log('am here' + swanthamWorkerId);
    apiLogGenerator(swanthamWorkerId, swanthamMachineId);
  })

  eventQueueService.chatLogListener(function(message) {
    chatLogGenerator(swanthamWorkerId, swanthamMachineId);
  })

  eventQueueService.incomingMessageListner(function(message) {
    incomingMessageLogGenerator(swanthamWorkerId, swanthamMachineId);
  })

  eventQueueService.filterMessageListner(function(message) {
    filterMessagesLogGenerator(swanthamWorkerId, swanthamMachineId);
  })

  eventQueueService.connectClientListener(function(data1) {
    let data = JSON.parse(data1);
    activeClientLocalListService.addNewClient(data.user, data.workerId, data.id, data.machineId);
  })

}




module.exports = {
  listenForAllEvents,
  connectClient,
  loggenerator,
  apiLog
}
