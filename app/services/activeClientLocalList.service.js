let activeClientLocalList = {};
function addNewClient(user, workerId, id, machineId) {

  activeClientLocalList[user]={
  	workerId, 
  	id, 
  	machineId
  };
  // console.log('new client',activeClientLocalList[user])
}
function getWorkerInfo(user) {
  return activeClientLocalList[user];
}
function removeClient(user) {
  delete activeClientLocalList[user]
}
function getCount() {
	return Object.keys(activeClientLocalList).length;
}
module.exports = {
  addNewClient,
  getWorkerInfo,
  removeClient,
  getCount
}