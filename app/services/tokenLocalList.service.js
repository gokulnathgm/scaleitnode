let tokenLocalList = {};
function addNewToken(name, token) {

  tokenLocalList[name]={
  	token
  };
  // console.log('new client',activeClientLocalList[user])
}
function getUserToken(user) {
  return tokenLocalList[user];
}
function removeToken(user) {
  delete tokenLocalList[user]
}

module.exports = {
  addNewToken: addNewToken,
  getUserToken: getUserToken,
  removeToken: removeToken,

}