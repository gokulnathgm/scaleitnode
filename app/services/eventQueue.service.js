const cluster = require('cluster');
//const request = require('redis-event-queue')({ host: "10.7.80.3", port: "6379" });
const request = require('redis-event-queue')();
let clientQueue = request.workqueue;
let broadcastQueue = request.broadcast;
const myArgs = process.argv.slice(2);
let workerId;


if (cluster.isWorker) {
  workerId = cluster.worker.id;
}

function generateChannel(event, id, machineId) {
  var channel = event + '_' + id + machineId;
  ////console.log(channel);
  return channel;
}


function emitEvent(event, workerId, machineId, data) {
  clientQueue.emit(generateChannel(event, workerId, machineId), data);
}

function listenEvent(event, workerId, machineId, callback) {
  clientQueue.on(generateChannel(event, workerId, machineId), callback);
}

// custom event listners
function listenCloseClient(workerId, machineId, closeClientCallback) {
  listenEvent('close_client', workerId, machineId, closeClientCallback);
}

function listenSendMessage(workerId, machineId, sendMessageCallback) {
  listenEvent('send_message', workerId, machineId, sendMessageCallback);
}

// custom event emiters
function emitCloseClient(user, workerId, id, machineId) {
  emitEvent('close_client', workerId, machineId, JSON.stringify({ user, id }));
}

function emitSendMessage(workerId, machineId, message) {
  ////console.log('am here inside emitSendMessage');
  emitEvent('send_message', workerId, machineId, message);
}


function apiLogListener(apiLogListenerCallback) {
  ////console.log('am here inside apiLogListener');
  broadcastQueue.on('apilog', apiLogListenerCallback);
}

function generateApiLog() {

  broadcastQueue.emit('apilog', 'generateApiLog');
}

function chatLogListener(chatLogListenerCallback) {

  broadcastQueue.on('chatLog', chatLogListenerCallback);
}

function generateChatLog() {
  broadcastQueue.emit('chatLog', 'generateChatLog');
}


function incomingMessageListner(incomingMessageListenerCallback) {
  broadcastQueue.on('incomingMessage', incomingMessageListenerCallback);
}

function generateincomingMessageLog() {
  broadcastQueue.emit('incomingMessage', 'generateIncomingMessageLog');
}


function filterMessageListner(filterMessageListenerCallback) {
  broadcastQueue.on('generatefilterMessage', filterMessageListenerCallback);
}

function generatefilterMessageLog() {
  broadcastQueue.emit('generatefilterMessage', 'generatefilterMessageLog');
}

function connectClientListener(connectClientListenerCallback) {
  // console.log('am here inside connectClientListener');
  broadcastQueue.on('connect_client', connectClientListenerCallback);
}

function emitConnectClient(user, workerId, id, machineId) {
// console.log('am here inside emitConnectClient',user, workerId, id, machineId);
  broadcastQueue.emit('connect_client', JSON.stringify({'user':user, 'workerId':workerId,'id':id, 'machineId':machineId}));
}

module.exports = {
  listenCloseClient,
  listenSendMessage,
  emitCloseClient,
  emitSendMessage,
  apiLogListener,
  chatLogListener,
  generateApiLog,
  generateChatLog,
  incomingMessageListner,
  generateincomingMessageLog,
  filterMessageListner,
  generatefilterMessageLog,
  connectClientListener,
  emitConnectClient
}
