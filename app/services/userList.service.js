const userService = require('../services/user.service');
let userList = {}
function initialiseList() {
	userService.findAll(function (err,data) {
    for (var i = 0; i < data.length; i++) {
    	userList[data[i].username] = data[i].password;
    }

  });
}
function addUser(username,password) {
	userList[username] = password;
}
function getPassword(username){
	return userList[username];
}
module.exports = {
  initialiseList:initialiseList,
  addUser:addUser,
  getPassword:getPassword

}
