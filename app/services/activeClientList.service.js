const redis = require("redis");
const bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
//const client = redis.createClient({ host: "10.7.80.3", port: "6379" });
const client = redis.createClient();


client.on('connect', function() {
  ////console.log('redis client started')
})

function addNewClient(user, workerId, id, machineId) {
  return client.hmsetAsync('activeClients', {
    [user]: JSON.stringify({ workerId, id, machineId })
  })
}

function getWorkerInfo(user) {
  return client.hgetAsync('activeClients', user);
}


function removeClient(user) {
  return client.hdelAsync('activeClients', user);
}


module.exports = {
  addNewClient,
  getWorkerInfo,
  removeClient
}
