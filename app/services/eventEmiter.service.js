const rabbitmq = require('rabbitmq-eventemitter');
const clientQueue = rabbitmq('amqp://localhost');

function connectClientListener(connectClientListenerCallback) {
  clientQueue.pull('connect_client', connectClientListenerCallback);
}

function emitConnectClient(user, instanceId) {
  clientQueue.push('connect_client', JSON.stringify({'user':user, 'instanceId':instanceId}));
}

function closeClientListener(closeClientListenerCallback) {
  clientQueue.pull('close_client', closeClientListenerCallback);
}

function emitCloseClient(user) {
  clientQueue.push('close_client', JSON.stringify({'user':user}));
}

function sendMessageListener(sendMessageListenerCallback) {
  clientQueue.pull('send_message', sendMessageListenerCallback);
}

function emitsendMessage(message,username) {
  clientQueue.push('send_message', JSON.stringify({'message':message, 'username':username}));
}

function newUserListener(newUserListenerCallback) {
  clientQueue.pull('new_user', newUserListenerCallback);
}

function emitnewUser(username,password) {
  clientQueue.push('new_user', JSON.stringify({'username':username, 'password':password}));
}

module.exports = {
  connectClientListener,
  emitConnectClient,
  closeClientListener,
  emitCloseClient,
  sendMessageListener,
  emitsendMessage,
  newUserListener,
  emitnewUser
}
