const User = require('../models/userSchema');
//////console.log(User)
function findOne(query, cb) {
	
  return User.findOne(query).exec(cb);

}

function findOnePromise(query) {
	return User.findOne(query).exec();
}
function findAll(cb) {
  return User.find().exec(cb);
}

function save(userdata, cb) {
  const createUser = new User();
  createUser.username = userdata.username;
  createUser.password = userdata.password;
  return createUser.save(cb)
}

module.exports = {
  findOne: findOne,
  findOnePromise: findOnePromise,
  save: save,
  findAll:findAll
}
