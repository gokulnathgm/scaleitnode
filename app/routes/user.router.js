const router = require('express').Router();
const userController = require('../controllers/user.controller');
////console.log(userController);


router.post('/login', userController.login);
router.post('/signup', userController.registration);
router.get('/log1',userController.log1);
router.get('/log2',userController.log2);
router.get('/log3', userController.log3);
router.get('/log4', userController.log4);
router.post('/logout', userController.logout);

module.exports = router;

