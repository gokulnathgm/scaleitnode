const userAPI = require('./user.router');
//const chatAPI = require('./chat.router');

module.exports = function(app) {
  app.use('/webchat', userAPI);
  //app.use('/chat', chatAPI);
};
