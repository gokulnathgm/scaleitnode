const userService = require('../services/user.service');
const testService = require('../services/test.service');
const tokenGenerationService = require('../services/tokenGeneration.service');
const eventEmiterService = require('../services/eventEmiter.service.js');
const userListService = require('../services/userList.service');

function login(req, res) {
  /*userService.findAll(function (err,data) {
    console.log(err,data[0].validPassword(password))
  });*/
  let recievedTime = new Date().getTime();
  const username = req.body.username;
  const password = req.body.password;
  const apiKey = req.body.api_key;
  let errorMessage = { 'status': 400, 'error_message': "Error" };
  if (apiKey !== 'qwertyuiop') {
    //console.log('apiKey missing');
    let sendTime = new Date().getTime();
    res.json(errorMessage);
  } else {
    if(userListService.getPassword(username)){
      if(userListService.getPassword(username)===password){
        let successMessage = { 'status': 200, 'action': 'login', 'message': "Login Successful", 'token': tokenGenerationService.generateToken(username) }
            let sendTime = new Date().getTime();
    
            res.json(successMessage);
      }else{
        let sendTime = new Date().getTime();
            res.json(errorMessage);
      }
    }else{
      let sendTime = new Date().getTime();
            res.json(errorMessage);
    }
    /**/
    /*userService.findOne({ 'username': username }, function(err, data) {
      if (err) {
        //console.log('Mongo db error');
        let sendTime = new Date().getTime();
        res.json(errorMessage);
        //testService.apiLog(req, recievedTime, sendTime, errorMessage, false);
      } else {
        if (!data) {
          //console.log('No user found');
          let sendTime = new Date().getTime();
          res.json(errorMessage);
          //testService.apiLog(req, recievedTime, sendTime, errorMessage, false);
        }
        if (data) {
          if (data.validPassword(password)) {
            //check whether the user already have an active token in action
            
            let successMessage = { 'status': 200, 'action': 'login', 'message': "Login Successful", 'token': tokenGenerationService.generateToken(username) }
            let sendTime = new Date().getTime();
    
            res.json(successMessage);

          
          } else {
            //console.log('Password error');
            let sendTime = new Date().getTime();
            res.json(errorMessage);
            //testService.apiLog(req, recievedTime, sendTime, errorMessage, false);
          }
        }
      }
    });
*/    
    
              //testService.apiLog(req, recievedTime, sendTime, successMessage, true);
  }
}

function registration(req, res) {
  //console.log(req.body);
  let recievedTime = new Date().getTime();
  const apiKey = req.body.api_key;
  const username = req.body.username;
  const password = req.body.password;
  let errorMessage = { 'status': 400, 'error_message': "Error" };
  let successMessage = { 'status': 200, 'action': 'signup', 'message': 'Sign Up Successful' };


  if (apiKey !== 'qwertyuiop') {
    let sendTime = new Date().getTime();
    res.json(errorMessage);
    //testService.apiLog(req, recievedTime, sendTime, errorMessage, false);
    //console.log('apiKey missing');

  } else {
    userService.findOne({ 'username': username }, function(err, data) {
      if (err) {
        //console.log('Mongo db error');
        let sendTime = new Date().getTime();
        res.json(errorMessage)
        //testService.apiLog(req, recievedTime, sendTime, errorMessage, false);
      } else {
        if (data) {
          //console.log('User already exist');
          let sendTime = new Date().getTime();
          res.json(errorMessage);
          //testService.apiLog(req, recievedTime, sendTime, errorMessage, false);
        }
        if (!data) {
          userService.save({
            'username': username,
            'password': password
          }, function(err, data) {
            if (err) {
              //console.log(err);
              //console.log('Mongo db chathichashaanee.....!!');
              let sendTime = new Date().getTime();
              res.json(errorMessage);
              //testService.apiLog(req, recievedTime, sendTime, errorMessage, false);
            } else {
              //console.log('Successfully registered');
              eventEmiterService.emitnewUser(username,password);
              let sendTime = new Date().getTime();
              res.json(successMessage)
              //testService.apiLog(req, recievedTime, sendTime, successMessage, true);
            }
          });
        }
      }
    });
  }
}




function logout(req, res) {
  const username = req.body.username;
  const apiKey = req.body.api_key;
  let errorMessage = { 'status': 400, 'action': 'logout', 'error': 'Logged out Error' };
  let successMessage = { 'status': 200, 'action': 'logout', 'message': 'Logged out Successfully' };

  if (apiKey !== 'qwertyuiop') {    
    res.json(errorMessage);
    //console.log('apiKey missing');
  }else{
    tokenService.getUserToken(username).then(function(activeToken) {
      if (activeToken === null) {
        res.json(errorMessage);
      }else{
        activeClientListService.getWorkerInfo(username).then(function(Ids) {
          let userIds = JSON.parse(Ids);
          if (Ids !== null) {
            eventQueueService.emitCloseClient(username, userIds.workerId, userIds.id);
          }
        });
        tokenService.removeToken(username).then(function(){
          res.json(successMessage);
        });
      }
    })
  }

}


//api log call
function log1(req, res) {
  eventQueueService.generateApiLog();
  res.send('Success');

}



//chat log call
function log2(req, res) {
  eventQueueService.generateChatLog();
  res.send('Success');
}

//check log
function log3(req, res) {
  eventQueueService.generateincomingMessageLog();
  res.send('Success');
}

function log4(req, res) {
  eventQueueService.generatefilterMessageLog();
  res.send('Success');
}



module.exports = {
  login: login,
  registration: registration,
  log1: log1,
  log2:log2,
  log3:log3,
  log4:log4,
  logout: logout
};
