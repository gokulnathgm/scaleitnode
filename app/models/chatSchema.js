var mongoose = require('mongoose');
var Schema = mongoose.Schema
var uniqueValidator = require('mongoose-unique-validator');
var timeStamps = require('mongoose-timestamp');

var chatSchema = new Schema({
  userId: {
    type: String
  },
  chat: {
    type: Array
  }
});

chatSchema.plugin(uniqueValidator);
chatSchema.plugin(timeStamps);
module.exports = mongoose.model('chatlog', chatSchema);