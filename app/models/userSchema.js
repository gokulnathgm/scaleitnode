const mongoose = require('mongoose');
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator');
const timeStamps = require('mongoose-timestamp');
const bcrypt = require('bcrypt-nodejs');
const registerSchema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

registerSchema.plugin(uniqueValidator);
registerSchema.plugin(timeStamps);

registerSchema.methods.encryptPassword = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

registerSchema.methods.validPassword = function(password) {
  return password===this.password;
};

module.exports = mongoose.model('User', registerSchema);
