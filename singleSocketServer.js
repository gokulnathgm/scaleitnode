const URL = require('url');
const WebSocketServer = require('ws').Server;
const http = require("http");
const cluster = require('cluster');

// REQUIRE CUSTUM SERVICES
const connectionListService = require('./app/services/connectionList.service.js');
const onlineClientListService = require('./app/services/onlineClientList.service.js');
const eventEmiterService = require('./app/services/eventEmiter.service.js');
const tokenGenerationService = require('./app/services/tokenGeneration.service');
// GET PORT NO FROM ARGS
const myArgs = process.argv.slice(2);
const port = myArgs[0];



// INITIALISE COUNT VARIABLES
let sendError = 0;
let noUser = 0;

// INSTANCE ID can be worker id or port no
let instanceId;
if (cluster.isWorker) {
  instanceId = cluster.worker.id;
}
// HTTP server
const server = http.createServer(function(req, res) {
    res.end("Hello");
});

// Handle upgrade from http to ws
server.on("upgrade", function(req, socket, head) {

    // Token verification
    if(iDontWantThemToBecomeAWebSocket(req)){
        socket.destroy(); //<-------------------------- Close Connection
    }
});

// WS server
const wss = new WebSocketServer({
    server: server
});

// Listen at port
server.listen(port);

// Handle Connection
wss.on('connection', function connection(senderConnection) {

    // Add to connection list
    let username = getUserNameFromToken(extractTokenFromUrl(senderConnection.upgradeReq.url));
    connectionListService.addConnection(username,senderConnection);
    notifyAllAboutConnection(username,instanceId);

    // Listner for message event
    senderConnection.on('message', function incoming(message) {

      let messageObject = JSON.parse(message);
      let messageToSend = JSON.stringify({
                  type: 'message',
                  data: {
                    text: messageObject.msg,
                    to: messageObject.to,
                    author: username,
                  }
                });
      
      if(isClientOnline(messageObject.to)){
        senderConnection.send(messageToSend);
        notifyAllAboutMessage(messageToSend,messageObject.to);
      }else{
        senderConnection.send(JSON.stringify({ type: 'alert', data: 'The user is not available' }));
        noUser++;
        console.log('noUser',noUser);
      }
      
    });

    // Listner for close event
    senderConnection.on('close', function closing(argument) {
      connectionListService.removeConnection(username)
      eventEmiterService.emitCloseClient(username);
      console.log(argument);
    });
});


//Utilities
function iDontWantThemToBecomeAWebSocket(req){
  let token = extractTokenFromUrl(req.url);
  console.log(token)
  return token ? false : true;
}

function extractTokenFromUrl(url){
  let token = URL.parse(url,true).query.token;
  return token;
}

function getUserNameFromToken(token){
  return tokenGenerationService.generateToken(token);
}

function sendMessageToUser(message,username) {
  let receiverConnection = connectionListService.getConnection(username);
  if(receiverConnection){
    receiverConnection.send(message,function(error) {
      if(error!==undefined){
        console.log('sendError')
      }
    });
  }  
}

function addClientToOnlineList(user, instanceId) {
  onlineClientListService.addNewClient(user, instanceId);
}

function removeClientFromOnlineList(user) {
  onlineClientListService.removeClient(user);
}

function isClientOnline(user) {
  return onlineClientListService.getClientInfo(user) ? true : false ;
}

function notifyAllAboutConnection(user, instanceId) {
  eventEmiterService.emitConnectClient(user, instanceId)
}

function notifyAllAboutMessage(message,username) {
  eventEmiterService.emitsendMessage(message,username)
}

function listenToConnectCLientEvents() {
  eventEmiterService.connectClientListener(function (clientInfo) {
    let clientInfoObject = JSON.parse(clientInfo);
    addClientToOnlineList(clientInfoObject.user,clientInfoObject.instanceId);
  })
}

function listenToSendMessage() {
  eventEmiterService.sendMessageListener(function (messageData) {
    let messageDataObject = JSON.parse(messageData);
    sendMessageToUser(messageDataObject.message,messageDataObject.username)
  })
}
listenToConnectCLientEvents();
listenToSendMessage();