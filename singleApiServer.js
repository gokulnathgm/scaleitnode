const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const mongoUrl = 'mongodb://localhost/nataraj';
const routes = require('./app/routes/index');

const userListService = require('./app/services/userList.service');
const eventEmiterService = require('./app/services/eventEmiter.service.js');

const myArgs = process.argv.slice(2);
const port = myArgs[0];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//setting routes
routes(app);

const server = app.listen(port, function() {
  console.log('Server Started '+port);  
});
mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl, function(err, res) {
  if (err) {
    console.log(err);
    console.log(' Mongo connection failed');
  } else {
    console.log(' Mongo connection success');
    userListService.initialiseList();
  }
});


function listenToNewUser() {
  eventEmiterService.newUserListener(function (data) {
    let dataObject = JSON.parse(data);
    userListService.addUser(dataObject.username,dataObject.password);
  })
}
listenToNewUser();