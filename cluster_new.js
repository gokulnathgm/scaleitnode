var clusterMaster = require("cluster-master");
clusterMaster({ exec: "untitled.js" // script to run 
              , size: 5 // number of workers 
              , env: { SOME: "environment_vars" }
              , args: [ "--deep", "doop" ]
              , silent: true
              , signals: false
              , onMessage: function (msg) {
                  console.error("Message from %s %j"
                               , this.uniqueID
                               , msg)
                }
              })